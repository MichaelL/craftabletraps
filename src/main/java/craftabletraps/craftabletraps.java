package craftabletraps;

import necesse.engine.modLoader.annotations.ModEntry;
import necesse.inventory.recipe.Ingredient;
import necesse.inventory.recipe.Recipe;
import necesse.inventory.recipe.Recipes;
import necesse.engine.registries.*;

@ModEntry
public class craftabletraps {

    public void init()
	{
        System.out.println("Craftable Traps Loaded");
    }

    public void postInit()
	{
        Recipes.registerModRecipe(new Recipe(
                "woodarrowtrap",
                1,
                RecipeTechRegistry.WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("woodwall", 1)
                }
        ).showAfter("woodwall"));
        Recipes.registerModRecipe(new Recipe(
                "stoneflametrap",
                1,
                RecipeTechRegistry.WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("stonewall", 1)
                }
        ).showAfter("stonewall"));
        Recipes.registerModRecipe(new Recipe(
                "stonearrowtrap",
                1,
                RecipeTechRegistry.WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("stonewall", 1)
                }
        ).showAfter("stoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "sandstoneflametrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("sandstonewall", 1)
                }
        ).showAfter("sandstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "sandstonearrowtrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("sandstonewall", 1)
                }
        ).showAfter("sandstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "swampstoneflametrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("swampstonewall", 1)
                }
        ).showAfter("swampstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "swampstonearrowtrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("swampstonewall", 1)
                }
        ).showAfter("swampstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "snowstonearrowtrap",
                1,
                RecipeTechRegistry.WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("snowstonewall", 1)
                }
        ).showAfter("snowstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "icearrowtrap",
                1,
                RecipeTechRegistry.WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("icewall", 1)
                }
        ).showAfter("snowstonearrowtrap"));
        Recipes.registerModRecipe(new Recipe(
                "dungeonflametrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("dungeonwall", 1)
                }
        ).showBefore("deepstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "dungeonarrowtrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("dungeonwall", 1)
                }
        ).showAfter("dungeonflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "dungeonvoidtrap",
                1,
                RecipeTechRegistry.DEMONIC,
                new Ingredient[]{
                        new Ingredient("voidshard", 5),
                        new Ingredient("dungeonwall", 1)
                }
        ).showAfter("dungeonarrowtrap"));
        Recipes.registerModRecipe(new Recipe(
                "deepstoneflametrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("deepstonewall", 1)
                }
        ).showAfter("deepstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "deepstonearrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("deepstonewall", 1)
                }
        ).showAfter("deepstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "obsidianflametrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("obsidianwall", 1)
                }
        ).showAfter("obsidianwall"));
        Recipes.registerModRecipe(new Recipe(
                "obsidianarrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("obsidianwall", 1)
                }
        ).showAfter("obsidianflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "deepsnowstoneflametrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("deepsnowstonewall", 1)
                }
        ).showAfter("deepsnowstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "deepsnowstonearrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("deepsnowstonewall", 1)
                }
        ).showAfter("deepsnowstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "deepswampstoneflametrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("deepswampstonewall", 1)
                }
        ).showAfter("deepswampstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "deepswampstonearrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("deepswampstonewall", 1)
                }
        ).showAfter("deepswampstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "deepsandstoneflametrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("firearrow", 10),
                        new Ingredient("deepsandstonewall", 1)
                }
        ).showAfter("deepsandstonewall"));
        Recipes.registerModRecipe(new Recipe(
                "deepsandstonearrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("deepsandstonewall", 1)
                }
        ).showAfter("deepsandstoneflametrap"));
        Recipes.registerModRecipe(new Recipe(
                "spidercastlearrowtrap",
                1,
                RecipeTechRegistry.ADVANCED_WORKSTATION,
                new Ingredient[]{
                        new Ingredient("stonearrow", 10),
                        new Ingredient("spidercastlewall", 1)
                }
        ).showBefore("stonearrow"));
    }

}
